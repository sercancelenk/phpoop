<?php
// Site Config
define(rootdir, "/phpoop");
if (!defined('root')) {
    define("root", $_SERVER['DOCUMENT_ROOT'] . "/phpoop");
}
// Web Service
define(SERVICE_ADDRESS, "212.68.63.234:8080");
define(SERVICE_PROJECT, "phprest");
define(SERVICE_MOD, "rest");
define(SERVICE_ALL, "all");
define(SERVICE_SAVE, "save");
define(SERVICE_SINGLE, "single");
define(SERVICE_DELETE, "delete");
define(SERVICE_LOGIN, "acc");

// COuntry Service //
define(ACCOUNT_SERVICE_MOD, "acc");



session_start();

/**
 * Description of AccountDAO_Class
 *
 * @author sercan
 */

//require_once $_SESSION['DOCUMENT_ROOT'] . '/constants/Constants.php';
//require_once 'GenericDAO_Class.php';
class GenericDAO {
    public $url;
    public $object;
    public $type;
    public $module;
    public $id;
    public $logger;
    
    public function __construct() {
        //Logger::configure('/opt/log4php/appender_dailyfile.properties');
        //$this->logger = Logger::getRootLogger();
    }
    
    public function __setUp(){
        $this->url = "http://" . SERVICE_ADDRESS . "/" . SERVICE_PROJECT . "/" . SERVICE_MOD . "/" . $this->type;// . "/" . $this-> module;
        if(!is_null($this->id)){
            $this->url = $this->url . "/" . $this->id;
        }
        if (!is_null($this->object)) {
            $this->object = $this->object;
        }
        echo "###################<br>".$this->url."#######################<br>";
    }
    
    
    public function getRequest(){
        $data = file_get_contents($this->url);
        $data = json_decode($data);
        
        if($data->valid){
            return $data->data;
        }
        else{
            return $data->errors;
        }
    }
    
    public function postRequest(){
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->object));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json','Content-Type: application/json'));
        curl_exec($ch);
        
        curl_close($ch);
        
    }
    
    public function deleteRequest(){
        echo $this->url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, DELETE);
        curl_exec($ch);
        curl_close($ch);
    }
    
}


class AccountDAO_Class extends GenericDAO{
    public function prepareService($module,$id,$object){
        $this->type = ACCOUNT_SERVICE_MOD;
        $this->module = $module;
        $this->id=$id;
        $this->object=$object;
        $this->__setUp();
        
    }
    
    public function getUser($id){
        $start=  microtime(true)*1000;
        //$id=$username."/".$password;
        $this->prepareService(null, $id, null);
        $account=$this->getRequest();
        var_dump($account);
        //$_SESSION['uid']=$account->id;
        //$_SESSION['uname']=$account->username;
        //$_SESSION['utype']=$account->type;
        $finish=  microtime(true)*1000;
        //$this->logger->info("Processing on AccountDAO::authentication($username) takes " . substr(($finish - $start), 0, 4) . " ms");
        echo $account->last_login_date;
    }
    
    public function authentication($username,$password){
        $start=  microtime(true)*1000;
        $id=$username."/".$password;
        $this->prepareService(SERVICE_LOGIN, $id, null);
        $account=$this->getRequest();
        var_dump($account);
        //$_SESSION['uid']=$account->id;
        //$_SESSION['uname']=$account->username;
        //$_SESSION['utype']=$account->type;
        $finish=  microtime(true)*1000;
        //$this->logger->info("Processing on AccountDAO::authentication($username) takes " . substr(($finish - $start), 0, 4) . " ms");
        echo $account->last_login_date;
    }
}

$accountDAO = new AccountDAO_Class();

$accountDAO->getUser("1");



?>