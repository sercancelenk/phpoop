<?php

// Site config

define(rootdir,"/phpoop");
if(!defined('root')){
    define("root",$_SERVER['DOCUMENT_ROOT']."/phpoop");
}

//Web Service
define(SERVICE_ADDRESS,"212.68.63.234:8080");
define(SERVICE_PROJECT,"phprest");
define(SERVICE_MOD,"rest");
define(SERVICE_ALL,"all");
define(SERVICE_SAVE,"save");
define(SERVICE_SINGLE,"single");
define(SERVICE_DELETE,"delete");
define(SERVICE_LOGIN,"login");

//LOGIN SERVICE
//define(LOGIN_SERVICE_MOD,"logins");

//ACCOUNT SERVICE
define(ACCOUNT_SERVICE_MOD,"acc")

?>
