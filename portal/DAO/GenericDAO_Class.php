<?php



/**
 * Description of GenericDAO_Class
 *
 * @author sercan
 */

//Logger ile ilgili islemleri burada yapabiliriz.
//require_once '/opt/log4php/src/main/php/Logger.php';


class GenericDAO {
    public $url;
    public $object;
    public $type;
    public $module;
    public $id;
    public $logger;
    
    public function __construct() {
        //Logger::configure('/opt/log4php/appender_dailyfile.properties');
        //$this->logger = Logger::getRootLogger();
    }
    
    public function __setUp(){
        $this->url = "http://" . SERVICE_ADDRESS . "/" . SERVICE_PROJECT . "/" . SERVICE_MOD . "/" . $this->type . "/" . $this-> module;
        if(!is_null($this->id)){
            $this->url = $this->url . "/" . $this->id;
        }
        if (!is_null($this->object)) {
            $this->object = $this->object;
        }
        //echo "###################<br>".$this->url."#######################<br>";
    }
    
    
    public function getRequest(){
        $data = file_get_contents($this->url);
        $data = json_decode($data);
        
        if($data->valid){
            return $data->data;
        }
        else{
            return $data->errors;
        }
    }
    
    public function postRequest(){
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->object));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json','Content-Type: application/json'));
        curl_exec($ch);
        
        curl_close($ch);
        
    }
    
    public function deleteRequest(){
        echo $this->url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, DELETE);
        curl_exec($ch);
        curl_close($ch);
    }
    
}

?>
