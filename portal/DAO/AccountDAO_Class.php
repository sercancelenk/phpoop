<?php

session_start();

/**
 * Description of AccountDAO_Class
 *
 * @author sercan
 */

require_once $_SESSION['DOCUMENT_ROOT'] . '/constants/Constants.php';
require_once 'GenericDAO_Class.php';

class AccountDAO_Class extends GenericDAO{
    public function prepareService($module,$id,$object){
        $this->type = ACCOUNT_SERVICE_MOD;
        $this->module = $module;
        $this->id=$id;
        $this->object=$object;
        $this->__setUp();
        
    }
    
    public function authentication($username,$password){
        $start=  microtime(true)*1000;
        $id=$username."/".$password;
        $this->prepareService(SERVICE_LOGIN, $id, null);
        $account=$this->getRequest();
        var_dump($account);
        $_SESSION['uid']=$account->id;
        $_SESSION['uname']=$account->username;
        $_SESSION['utype']=$account->type;
        $finish=  microtime(true)*1000;
        //$this->logger->info("Processing on AccountDAO::authentication($username) takes " . substr(($finish - $start), 0, 4) . " ms");
        
    }
}

?>
